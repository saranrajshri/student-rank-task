import sys

data={}
names=[]
n=int(input())
new_data={}
def getData():
	for _ in range(n):
		name=str(input())
		data[name]=''
		names.append(name)

	for i in range(n):
		tempList=map(int,input().split())
		tempList=list(tempList)

		# calcutate total
		normalTotal=tempList[1]+tempList[2]+tempList[3]+tempList[4]+tempList[5]+tempList[6]
		boostMark=0
		if tempList[7] >=95 and tempList[7]<=100:
			boostMark=50
		elif tempList[7] >=80 and tempList[7]<=94:
			boostMark=45
		elif tempList[7]>=60 and tempList[7] <=79:
			boostMark=40
		elif tempList[7] >=50 and tempList[7]<=59:
			boostMark=35
		elif tempList[7]>=40 and tempList[7]<=49:
			boostMark=30
		elif tempList[7]<=40:
			boostMark=25

		tempList.append(normalTotal+boostMark)
		data[names[i]]=tempList


def printToppersData():
	print("=========")
	toppersIteratingVar=1
	toppersRank=0
	lastTopperTotal=0
	continousToppersRankCount=0

	for key,val in sorted(data.items(),key=lambda e:e[1][8],reverse=True):	
		total=val[8]
		reg_no=val[0]

		if(toppersIteratingVar !=4):
			# compare the last person total
			if(lastTopperTotal==total):
				continousToppersRankCount+=1
			else:
				if(continousToppersRankCount>0):
					toppersRank+=continousToppersRankCount+1
				else:
					toppersRank+=1
				continousToppersRankCount=0
			print(reg_no,key,total,toppersRank)
			# print("count>=",continousToppersRankCount)	
		else:
			break
		toppersIteratingVar+=1

		lastTopperTotal=total



def  printFailuresData():
	print("===Fail=====")
	failuresIteratingVar=1
	failuresRank=0
	lastFailureTotal=0
	continousFailuresRankCount=0
	printFromWhich=n-5

	for key,val in sorted(data.items(),key=lambda e:e[1][8],reverse=True):
		total=val[8]
		reg_no=val[0]

		#compare last person total 
		if(lastFailureTotal==total):
			continousFailuresRankCount+=1
		else:
			if(continousFailuresRankCount>0):
				failuresRank+=continousFailuresRankCount+1
			else:
				failuresRank+=1
			continousFailuresRankCount=0

		if(failuresIteratingVar >= printFromWhich):

			print(reg_no,key,total,failuresRank)
			# print("count>=",continousToppersRankCount)	

		failuresIteratingVar+=1
		lastFailureTotal=total

def printAllData(data):
	print("===All==")
	toppersIteratingVar=1
	toppersRank=0
	lastTopperTotal=0
	continousToppersRankCount=0

	for key,val in sorted(data.items(),key=lambda e:e[1][8],reverse=True):	
		total=val[8]
		reg_no=val[0]

		if(toppersIteratingVar !=n+1):
			# compare the last person total
			if(lastTopperTotal==total):
				continousToppersRankCount+=1
			else:
				if(continousToppersRankCount>0):
					toppersRank+=continousToppersRankCount+1
				else:
					toppersRank+=1
				continousToppersRankCount=0

			# add the rank to the list
			val.append(toppersRank)
			new_data[key]=val

			print(reg_no,key,total,toppersRank)
			# print("count>=",continousToppersRankCount)	
		else:
			break
		toppersIteratingVar+=1

		lastTopperTotal=total

def showDict():
	print(new_data)

def showOptions():
	print("1.Search By Name")
	print("2.Search By Regno")
	print("3.Search By TotalMark")
	print("4.Search By Rank")
	print("5.Search By Attendence")
	print("6.Update Data")
	print("7.Exit")
	print("==========")
	print("Enter Your Option : ")
	option=int(input())

	# call the function
	if(option==1):
		searchByName()
	elif option==2:
		searchByRegno()
	elif option==3:
		searchByTotalMark()
	elif option==4:
		searchByRank()
	elif option==5:
		searchByAttendence()
	elif option==6:
		updateData()
	else:
		sys.exit()			

def showDetails(key,val):
	print(val)
	normalTotal=val[1]+val[2]+val[3]+val[4]+val[5]+val[6]
	boostMark=val[8]-normalTotal
	print("\n")
	print("Name :",key)
	print("Regno :",val[0])
	print("Sub1 :",val[1])
	print("Sub2 :",val[2])
	print("Sub3 :",val[3])
	print("Sub4 :",val[4])
	print("Sub5 :",val[5])
	print("Sub6 :",val[6])
	print("Attendence :",val[7])
	print("Boost Mark :",boostMark)
	print("TotalMark :",val[8])
	print("Rank :",val[9])
	print("====RESULTS [1] =======")
	showOptions()

def showDetailsForMultiple(tempData):
	print("\n")
	print(tempData)
	for i in range(len(tempData)):
		subArray=tempData[i]
		normalTotal=subArray[1][1]+subArray[1][2]+subArray[1][3]+subArray[1][4]+subArray[1][5]+subArray[1][6]
		boostMark=subArray[1][8]-normalTotal
		print("Name :",subArray[0])
		print("Regno :",subArray[1][0])
		print("Sub1 :",subArray[1][1])
		print("Sub2 :",subArray[1][2])
		print("Sub3 :",subArray[1][3])
		print("Sub4 :",subArray[1][4])
		print("Sub5 :",subArray[1][5])
		print("Sub6 :",subArray[1][6])
		print("Attendence :",subArray[1][7])
		print("Bonus :",boostMark)
		print("TotalMark :",subArray[1][8])
		print("Rank :",subArray[1][9])
		
	print("========= [{0}] RESULTS FOUND ========".format(len(tempData)))
	showOptions()

def showNoDataFound():
	print("No Data Found")
	print("===========")
	showOptions()

def searchByName():
	tempData=[]
	found=False
	name=str(input())
	for key,val in new_data.items():
		if key==name or name in key:
			data=[key,val]
			tempData.append(data)
			found=True
	if(found==True):
		showDetailsForMultiple(tempData)
	else:
		showNoDataFound()

def searchByRegno():
	found=False
	reg=int(input())
	for key,val in new_data.items():
		if val[0]==reg:
			found=True
			break
	if(found==True):
		showDetails(key,val)
	else:
		showNoDataFound()

def searchByTotalMark():
	tempData=[]
	found=False
	mark=int(input())
	for key,val in new_data.items():
		if mark==val[8]:
			data=[key,val]
			tempData.append(data)
			found=True
	if(found==True):
		showDetailsForMultiple(tempData)	
	else:
		showNoDataFound()

def searchByRank():
	found=False
	tempData=[]
	rank=int(input())
	for key,val in new_data.items():
		if rank==val[9]:
			data=[key,val]
			tempData.append(data)
			found=True

	if(found==True):

		showDetailsForMultiple(tempData)
	else:
		showNoDataFound()

def searchByAttendence():
	found=False
	tempData=[]
	attendence=int(input())
	for key,val in new_data.items():
		if val[7]==attendence:
			data=[key,val]
			tempData.append(data)
			found=True
	if(found==True):		
		showDetailsForMultiple(tempData)
	else:
		showNoDataFound()

def updateData():
	reg_no=int(input("Enter The Reg No:"))
	for key,val in new_data.items():
		if val[0]==reg_no:
			showUpdateOptions(reg_no)
			

def showUpdateOptions(regNo):
	print("1.Change Name:")
	print("2.Change Attendence")
	print("3.Sub 1")
	print("4.Sub 2")
	print("5.Sub 3")
	print("6.Sub 4")
	print("7.Sub 5")
	print("8.Sub 6")

	getOption=int(input("Enter Your Option :"))

	if getOption==1:
		updateName(regNo)
	elif getOption==2:
		updateAttendence(regNo)
	elif getOption==3:
		updateSub1(regNo)
	elif getOption==4:
		updateSub2(regNo)
	elif getOption==5:
		updateSub3(regNo)
	elif getOption==6:
		updateSub4(regNo)
	elif getOption==7:
		updateSub5(regNo)
	elif getOption==8:
		updateSub6(regNo)

def showUpdated():
	print("Updated !!")
	print("=======New Data======")
	printAllData(new_data)
	print("================")
	showOptions()
def calcRank(key,tempList):
	normalTotal=tempList[1]+tempList[2]+tempList[3]+tempList[4]+tempList[5]+tempList[6]
	boostMark=0
	if tempList[7] >=95 and tempList[7]<=100:
		boostMark=50
	elif tempList[7] >=80 and tempList[7]<=94:
		boostMark=45
	elif tempList[7]>=60 and tempList[7] <=79:
		boostMark=40
	elif tempList[7] >=50 and tempList[7]<=59:
		boostMark=35
	elif tempList[7]>=40 and tempList[7]<=49:
		boostMark=30
	elif tempList[7]<=40:
		boostMark=25

	tempList[8]=normalTotal+boostMark
	new_data[key]=tempList
	# show all rank
	printAllData(new_data)
def updateName(regNo):
	for key,val in new_data.items():
		if val[0]==regNo:
			newName=str(input("Enter the New Name:"))
			new_data[newName]=new_data[key]
			del new_data[key]
			showUpdated()
			break

def updateAttendence(regNo):
	for key,val in new_data.items():
		if val[0]==regNo:
			newInput=int(input("Enter the New Attendence:"))
			val[7]=newInput
			calcRank(key,val)
			showUpdated()
			break

def updateSub1(regNo):
	for key,val in new_data.items():
		if val[0]==regNo:
			while True:
				newInput=int(input("Enter the New Mark:"))
				if newInput<=100:
					break
				elif newInput>100:
					print("Invalid Mark")
			val[1]=newInput
			calcRank(key,val)
			showUpdated()
			break

def updateSub2(regNo):
	for key,val in new_data.items():
		if val[0]==regNo:
			newInput=int(input("Enter the New Sub2:"))
			while True:
				newInput=int(input("Enter the New Mark:"))
				if newInput<=100:
					break
				elif newInput>100:
					print("Invalid Mark")
			val[2]=newInput
			calcRank(key,val)
			showUpdated()
			break


def updateSub3(regNo):
	for key,val in new_data.items():
		if val[0]==regNo:
			newInput=int(input("Enter the New Sub3:"))
			while True:
				newInput=int(input("Enter the New Mark:"))
				if newInput<=100:
					break
				elif newInput>100:
					print("Invalid Mark")
			val[3]=newInput
			calcRank(key,val)
			showUpdated()
			break

def updateSub4(regNo):
	for key,val in new_data.items():
		if val[0]==regNo:
			newInput=int(input("Enter the New Sub4:"))
			while True:
				newInput=int(input("Enter the New Mark:"))
				if newInput<=100:
					break
				elif newInput>100:
					print("Invalid Mark")
			val[4]=newInput
			calcRank(key,val)
			showUpdated()
			break

def updateSub5(regNo):
	for key,val in new_data.items():
		if val[0]==regNo:
			newInput=int(input("Enter the New Sub5:"))
			while True:
				newInput=int(input("Enter the New Mark:"))
				if newInput<=100:
					break
				elif newInput>100:
					print("Invalid Mark")
			val[5]=newInput
			calcRank(key,val)
			showUpdated()
			break

def updateSub6(regNo):
	for key,val in new_data.items():
		if val[0]==regNo:
			newInput=int(input("Enter the New Sub7:"))
			while True:
				newInput=int(input("Enter the New Mark:"))
				if newInput<=100:
					break
				elif newInput>100:
					print("Invalid Mark")
			val[6]=newInput
			calcRank(key,val)
			showUpdated()
			break

getData()
printAllData(data)
showOptions()